use sdl2::rect::Rect;
use sdl2::pixels::Color;
use sdl2::render::Canvas;
use sdl2::video::Window;
use sdl2::EventPump;
use sdl2::event;

use super::rmath::Point;

pub struct Target {
  pub width: u32,
  pub height: u32,
  pub bg_color: Color,
  pub event_pump: EventPump,
  pub c: Canvas<Window>
}

impl Target {

  pub fn new(width: u32, height: u32, bg_color: Color) -> Target {
    let sdl = sdl2::init().unwrap();

    let canvas = sdl.video()
      .expect("Failed to get video subsystem")
      .window("Game", width, height)
      .resizable()
      .build()
      .expect("Failed to get window")
      .into_canvas()
      .build()
      .expect("Failed to build canvas for window");
    let event_pump = sdl.event_pump().expect("Failed to get event pump");

    Target {
      width,
      height,
      bg_color,
      event_pump,
      c: canvas
    }
  }

  fn logical_pt_to_pixel(&self, pt: Point) -> sdl2::rect::Point {
    (
      (pt.x * self.width as f32) as i32,
      ((1.0-pt.y) * self.height as f32) as i32
    ).into()
  }

  pub fn reset(&mut self) {
    self.c.set_draw_color(self.bg_color);
    self.c.fill_rect(Rect::new(0, 0, self.width, self.height)).unwrap();
  }

  pub fn set_color(&mut self, clr: Color) {
    self.c.set_draw_color(clr);
  }

  pub fn draw_line(&mut self, a: Point, b: Point) {
    let p1 = self.logical_pt_to_pixel(a);
    let p2 = self.logical_pt_to_pixel(b);
    self.c.draw_line(p1, p2).unwrap();
  }

  pub fn present(&mut self) {
    self.c.present();
  }

  pub fn poll(&mut self) -> bool {
    for event in self.event_pump.poll_iter() {
      match event {
        event::Event::Quit {..} => return true,
        _ => {}
      };
    }
    return false;
  }

  // pub fn draw_line<P1: Into<Point>, P2: Into<Point>>(&mut self, p1: P1, p2: P2, thickness: u8) {
  //   let a: Point = p1.into();
  //   let b: Point = p2.into();
  //   for i in 0..thickness {
  //     let o = i as i32;
  //     self.c.draw_line(a + (0, o).into(), b + (0, o).into()).unwrap();
  //     self.c.draw_line(a - (0, o).into(), b - (0, o).into()).unwrap();
  //     self.c.draw_line(a + (o, 0).into(), b + (o, 0).into()).unwrap();
  //     self.c.draw_line(a - (o, 0).into(), b - (o, 0).into()).unwrap();
  //   }
  // }

  // pub fn draw_ngon(
  //   &mut self,
  //   nsides: u8,
  //   radius: f32,
  //   include_spokes: bool,
  //   center: Option<(f32, f32)>,
  //   orientation: Option<Angle>,
  //   color: Option<Color>,
  //   thickness: Option<u8>) -> Result<(), String>  {

  //   if let Some(c) = color {
  //     self.c.set_draw_color(c);
  //   } else {
  //     self.c.set_draw_color(self.fg_color);
  //   }

  //   let thick = thickness.unwrap_or(1);

  //   let c: Vector = if let Some(center_pt) = center {
  //     center_pt.into()
  //   } else {
  //     ((self.width as f32)/ 2.0, (self.height as f32)/ 2.0).into()
  //   };

  //   let theta = ((2 as f32) * std::f32::consts::PI) / (nsides as f32);
  //   let rmat = Mat::rotation(theta);

  //   let mut offset = (radius, 0.0).into();

  //   if let Some(angle) = orientation {
  //     offset = Mat::rotation(angle).mul_vec(offset);
  //   }

  //   let mut points = Vec::new();

  //   let mut p1 = c + offset;
  //   for _ in 0..nsides {
  //     offset = rmat.mul_vec(offset);
  //     let p2 = c + offset;
  //     self.draw_line(p1, p2, thick);
  //     if include_spokes {
  //       self.c.draw_line(c, p1)?;
  //     }
  //     points.push(p1);
  //     p1 = p2;
  //   }
  //   self.c.set_draw_color(self.fg_color);
  //   for p in points {
  //     self.c.draw_point(p).unwrap();
  //   }

  //   Ok(())
  // }

  // pub fn draw_diag_grid(&mut self, mut divisions: u8, thickness: Option<u8>) -> Result<(), String> {
  //   let thick = thickness.unwrap_or(1);

  //   divisions = divisions / 2;
  //   self.c.set_draw_color(self.fg_color);
  //   let w = self.width as i32;
  //   let h = self.width as i32;
  //   let block_w = (w as f32)/(divisions as f32);
  //   let block_h = (h as f32)/(divisions as f32);
  //   for i in 0..divisions {
  //     let x = ((i as f32) * block_w) as i32;
  //     let y = ((i as f32) * block_h) as i32;

  //     self.draw_line((0 + x, h    ), (w    , 0 + y), thick);
  //     self.draw_line((0    , h - y), (w - x, 0    ), thick);
  //     self.draw_line((0 + x, 0    ), (w    , h - y), thick);
  //     self.draw_line((0    , 0 + y), (w - x, h    ), thick);
  //   }
  //   Ok(())
  // }

  // pub fn draw_grid(&mut self, divisions: u8, thickness: Option<u8>) -> Result<(), String> {
  //   let thick = thickness.unwrap_or(1);
  //   self.c.set_draw_color(self.fg_color);
  //   let block_w = (self.width as f32)/(divisions as f32);
  //   let block_h = (self.height as f32)/(divisions as f32);

  //   for i in 1..divisions {
  //     let y = ((i as f32) * block_h) as i32;
  //     let x = ((i as f32) * block_w) as i32;

  //     self.draw_line((x, 0), (x, self.height as i32), thick);
  //     self.draw_line((0, y), (self.width as i32, y), thick);
  //   }
  //   Ok(())
  // }

  // pub fn draw_bg(&mut self) -> Result<(), String> {
  //   self.c.set_draw_color(self.bg_color);
  //   self.c.fill_rect(Rect::new(0, 0, self.width, self.height))?;
  //   Ok(())
  // }

}
