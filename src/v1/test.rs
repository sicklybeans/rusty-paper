use sdl2::pixels::Color;
use super::core::*;
use super::target::*;
use super::draw1::*;

pub fn main() {
  let folds = vec![
    BasicFold {
      ftype: FoldType::Valley,
      point1: (0.66, 0.0).into(),
      point2: (0.66, 1.0).into()
    }
  ];
  let mut engine = Engine::new();
  engine.add_fold(BasicFold {
    ftype: FoldType::Valley,
    point1: (0.66, 0.0).into(),
    point2: (0.66, 1.0).into()
  });

  let target = Target::new(800, 800, Color::RGB(250, 250, 250));
  let runner = Runner::new(engine, target);
  runner.run();
}
