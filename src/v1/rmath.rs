use core::ops::Sub;
use core::ops::Mul;

pub type Pr = f32;

#[derive(Clone, Copy)]
pub struct Point {
  pub x: Pr,
  pub y: Pr
}

#[derive(Clone, Copy)]
pub struct Vector {
  pub x: Pr,
  pub y: Pr
}

impl Vector {
  pub fn norm(&self) -> Pr {
    (self.x * self.x + self.y * self.y).sqrt()
  }
}

impl Sub for Point {
  type Output = Vector;

  fn sub(self, p2: Self) -> Vector {
    Vector {
      x: self.x - p2.x,
      y: self.y - p2.y
    }
  }
}

impl Mul for Vector {
  type Output = Pr;

  fn mul(self, v2: Self) -> Pr {
    self.x * v2.x + self.y * v2.y
  }
}

pub fn get_angle(a: Point, b: Point, c: Point) -> Pr {
  let v1 = b - a;
  let v2 = b - c;
  ((v1 * v2) / (v1.norm() * v2.norm())).acos()
}

impl From<(Pr, Pr)> for Point {
  fn from(xy: (Pr, Pr)) -> Point {
    Point { x: xy.0, y: xy.1 }
  }
}

pub type Edge = (Point, Point);

fn between(a: Pr, b: Pr, c: Pr) -> bool {
  (a <= b && b <= c) || (c <= b && b <= a)
}

pub fn intersect(e1: &Edge, e2: &Edge) -> Option<Point> {
  // handle infinite slope case
  let m1 = (e1.1.y - e1.0.y)/(e1.1.x - e1.0.x);
  let m2 = (e2.1.y - e2.0.y)/(e2.1.x - e2.0.x);
  if m1 == m2 {
    return None;
  }
  let (x1, y1) = (e1.0.x, e1.0.y);
  let (x2, y2) = (e2.0.x, e2.0.y);

  // Find x position where the lines intersect
  let xi = (m1*x1 - m2*x2)+y2 - y1;

  if between(e1.0.x, xi, e1.1.x) && between(e2.0.x, xi, e2.1.x) {
    Some((xi, m1*(xi-x1) + y1).into())
  } else {
    None
  }
}

pub fn reflect(point: &Point, edge: &Edge) -> Point {
  let xp = point.x;
  let yp = point.y;
  let xe = edge.0.x;
  let ye = edge.0.y;

  if edge.1.x == edge.0.x {
    (xe - (xp - xe), yp).into()
  } else if edge.1.y == edge.0.y {
    (xp, ye - (yp - ye)).into()
  } else {
    let me = (edge.1.y - edge.0.y)/(edge.1.x - edge.0.x);
    let mp = - 1.0 / me;
    let xi = (xp*mp-xe*me+ye-yp)/(mp-me);
    let yi = me*(xi-xe)+ye;
    let dis = (Point::from((xi, yi)) - *point).norm();
    let x = xp + 2.0*(xi-xe);
    let y = mp*(x - xp) + yp;
    (x, y).into()
  }
}

