use sdl2::pixels::Color;
use std::borrow::Borrow;
use std::f32::consts::PI;
use super::core::Drawer;
use super::target::Target;
use super::rmath::*;

#[derive(Clone, Copy)]
pub enum Chirality {
  CW,
  CCW
}

impl Chirality {
  pub fn invert(&self) -> Chirality {
    match self {
      Chirality::CW => Chirality::CCW,
      Chirality::CCW => Chirality::CW
    }
  }
}

pub enum FoldType {
  Mountain,
  Valley
}

pub struct Polygon {
  points: Vec<Point>,
  chirality: Chirality
}

impl Polygon {

  pub fn new<T: Into<Point>>(chirality: Chirality, points: Vec<T>) -> Polygon {
    let p = Polygon {
      chirality,
      points: points.into_iter().map(|p| p.into()).collect()
    };
    if !p.check_valid() {
      panic!("Invalid polygon");
    }
    p
  }

  pub fn pt(&self, mut i: i32) -> Point {
    let n = self.points.len() as i32;
    if n == 0 {
      panic!("Polygon is empty!");
    }

    i = match self.chirality {
      Chirality::CW => i,
      Chirality::CCW => n - i - 1
    };

    while i < 0 {
      i += n;
    }
    while i >= n {
      i -= n;
    }
    self.points[i as usize]
  }

  pub fn draw(&self, target: &mut Target, c1: &Color, c2: &Color) {
    match self.chirality {
      Chirality::CW => target.set_color(*c1),
      Chirality::CCW => target.set_color(*c2)
    };
    for i in 0..self.points.len() {
      target.draw_line(self.pt(i as i32), self.pt((i+1) as i32));
    }
  }

  // pub fn get_edges(&self) -> Vec<Edge> {
  //   let mut p1 = self.points[self.points.len() - 1];
  //   return self.points.iter().map(|p| {
  //     let edge = (p1, *p);
  //     p1 = *p;
  //     edge
  //   }).collect()
  // }

  fn check_valid(&self) -> bool {
    if self.points.len() < 3 {
      return false
    }
    for i in 0..self.points.len() {
      let ind = i as i32;
      let angle = get_angle(self.pt(ind-1), self.pt(ind), self.pt(ind+1));
      if angle < 0.0 {
        return false;
      }
      if angle > PI {
        return false
      }
    }
    true
  }
}

// impl<T: Into<Point>> TryFrom<Vec<T>> for Polygon {
//   type Error = &'static str;

//   fn try_from(points: Vec<T>) -> Result<Polygon, Self::Error> {
//     let p = Polygon { points: points.into_iter().map(|p| p.into()).collect() };
//     if p.check_valid() {
//       Ok(p)
//     } else {
//       Err("Invalid polygon")
//     }
//   }
// }

pub struct BasicFold {
  pub ftype: FoldType,
  pub point1: Point,
  pub point2: Point
}

pub struct Engine {
  pub folds: Vec<BasicFold>,
}

impl Engine {
  pub fn new() -> Engine {
    Engine {
      folds: Vec::new()
    }
  }
  pub fn add_fold(&mut self, fold: BasicFold) {
    self.folds.push(fold);
  }
}

fn fold_face(face: &Polygon, fold: &BasicFold) -> Option<(Polygon, Polygon)> {
  let fold_edge = (fold.point1, fold.point2);

  let mut intersections: Vec<(usize, Point)> = Vec::new();

  for i in 0..face.points.len() {
    let edge = (face.pt(i as i32), face.pt((i+1) as i32));

    if let Some(point) = intersect(&fold_edge, &edge) {
      intersections.push((i, point));
    }
  }

  if intersections.len() == 0 {
    return None
  } else if intersections.len() != 2 {
    panic!("This should never happen with convex polygons");
  }
  let mut pts1 = Vec::new();
  let mut pts2 = Vec::new();

  let inter1 = intersections[0];
  let inter2 = intersections[1];

  for i in 0..face.points.len() {
    let ind = i as i32;
    if i <= inter1.0 || i >= inter2.0 {
      pts1.push(face.pt(ind));
    }
    if i == inter1.0 {
      pts1.push(inter1.1);
      pts2.push(inter1.1);
    }
    if i >= inter1.0 && i <= inter2.0 {
      pts2.push(face.pt(ind));
    }
    if i == inter2.0 {
      pts1.push(inter1.1);
      pts2.push(inter1.1);
    }
  }

  let poly1 = Polygon::new(face.chirality, pts1);
  let poly2 = Polygon::new(face.chirality.invert(), pts2);
  Some((poly1, poly2))
}

impl Drawer for Engine {

  fn draw(&mut self, target: &mut Target, frame: usize) {
    target.reset();
    let mut stacks: Vec<Polygon> = Vec::new();
    stacks.push(Polygon::new(Chirality::CW, vec![(0.0, 0.0), (0.0, 1.0), (1.0, 1.0), (1.0, 0.0)]));

    for fold in self.folds.iter() {
      let n = stacks.len();

      for i in 0..n {
        let face = stacks[n-i-1].borrow();
        if let Some((p1, p2)) = fold_face(face, fold) {
          stacks[n-i-1] = p1;
          stacks.push(p2)
        }
      }
    }

    let c1 = Color::RGB(250, 50, 50);
    let c2 = Color::RGB(50, 250, 50);

    for poly in stacks.iter() {
      poly.draw(target, &c1, &c2);
    }
  }
}

