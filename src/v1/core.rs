use std::time::{Duration, Instant};
use super::target::Target;

pub trait Drawer {
  fn draw(&mut self, target: &mut Target, frame: usize);
}

pub struct Runner<T: Drawer> {
  drawer: T,
  target: Target
}

impl<T: Drawer> Runner<T> {
  pub fn new(drawer: T, target: Target) -> Runner<T> {
    Runner { drawer, target }
  }
  pub fn run(mut self) {
    let sleep_time = Duration::new(0, 1_000_000_000u32 / 8);
    let mut frame = 0;

    self.target.present();

    loop {
      let loop_start = Instant::now();
      self.drawer.draw(&mut self.target, frame);
      self.target.present();
      if self.target.poll() {
        break;
      }
      let elapsed_time = loop_start.elapsed();

      frame += 1;

      if let Some(sleep_time) = sleep_time.checked_sub(elapsed_time) {
        std::thread::sleep(sleep_time);
      } else {
        println!("Cant keep up!");
      }
    }
  }
}
