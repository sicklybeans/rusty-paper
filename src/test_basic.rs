use sdl2::pixels::Color;
use std::time::{Duration, Instant};
use std::f32::consts::PI;
use crate::model::Model;
use crate::events;

fn draw_basic_background(m: &mut Model) {
  m.draw_bg().unwrap();
  m.draw_grid(16, Some(1)).unwrap();
  m.draw_grid(8, Some(2)).unwrap();
  m.draw_grid(4, Some(3)).unwrap();
  m.draw_grid(2, Some(4)).unwrap();
  m.draw_diag_grid(16, Some(1)).unwrap();
  m.draw_diag_grid(8, Some(2)).unwrap();
  m.draw_diag_grid(4, Some(3)).unwrap();
  m.draw_diag_grid(2, Some(4)).unwrap();
}

fn draw_triangle_diagram(m: &mut Model) {
  draw_basic_background(m);
  let radius = 800.0 / (3.0 as f32).sqrt();
  let center = (800.0/2.0, 800.0/2.0 + 100.0);
  let orientation = - PI / 2.0;
  m.draw_ngon(
    3,
    radius,
    true,
    Some(center),
    Some(orientation),
    Some(Color::RGB(0, 200, 100)),
    Some(5)).unwrap();
}

fn draw_pentagon_diagram(m: &mut Model) {
  draw_basic_background(m);
  let max_radius = 800.0 / (2.0 * (3.0 * PI / 5.0).sin());
  let radius = 400.0;
  let center = Some((800.0/2.0, radius));
  let orientation = - PI / 2.0;
  m.draw_ngon(
    5,
    radius,
    true,
    center,
    Some(orientation),
    Some(Color::RGB(0, 0, 0)),
    Some(1)).unwrap();
}


fn draw_stuff(m: &mut Model) {
  draw_pentagon_diagram(m);
}

pub fn test() {
  let mut md = Model::new(
    800, 800, Color::RGB(250, 250, 250), Color::RGB(100, 0, 50));
  draw_stuff(&mut md);
  md.present();

  let sleep_time = Duration::new(0, 1_000_000_000u32 / 8);
  let mut _frame = 0;

  loop {
    let loop_start = Instant::now();
    draw_stuff(&mut md);
    md.present();
    // square_lines(&mut canvas, Color::RGB(100, 0, 50), 1).unwrap();
    if events::poll(&mut md.event_pump) {
      break;
    }
    let elapsed_time = loop_start.elapsed();
    _frame += 1;

    if let Some(sleep_time) = sleep_time.checked_sub(elapsed_time) {
      std::thread::sleep(sleep_time);
    } else {
      println!("Cant keep up!");
    }
  }
}
