use sdl2::event;

pub fn poll(event_pump: &mut sdl2::EventPump) -> bool {
  for event in event_pump.poll_iter() {
    match event {
      event::Event::Quit {..} => return true,
      _ => {}
    };
  }
  return false;
}

