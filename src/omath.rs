use sdl2::rect::Point;
use core::ops::Sub;
use core::ops::Mul;
use core::ops::Add;

#[derive(Copy, Clone, Debug)]
pub struct Vector {
  pub x: f32,
  pub y: f32
}

#[derive(Debug)]
pub struct Mat {
  pub v1: Vector,
  pub v2: Vector
}

pub type Angle = f32;

impl From<(f32, f32)> for Vector {
  fn from(t: (f32, f32)) -> Vector {
    Vector {
      x: t.0,
      y: t.1
    }
  }
}

impl<V1, V2> From<(V1, V2)> for Mat
  where V1: Into<Vector>,
        V2: Into<Vector> {
  fn from(t: (V1, V2)) -> Mat {
    Mat {
      v1: t.0.into(),
      v2: t.1.into()
    }
  }
}

impl Add for Vector {
  type Output = Self;

  fn add(self, v2: Self) -> Self {
    (self.x + v2.x, self.y + v2.y).into()
  }
}

impl Sub for Vector {
  type Output = Self;

  fn sub(self, v2: Self) -> Self {
    (self.x - v2.x, self.y - v2.y).into()
  }
}

impl Mul for Vector {
  type Output = f32;

  fn mul(self, v2: Self) -> f32 {
    self.x * v2.x + self.y * v2.y
  }
}

impl Add for Mat {
  type Output = Self;

  fn add(self, m2: Self) -> Self {
    (self.v1 + m2.v1, self.v2 + m2.v2).into()
  }
}

impl Sub for Mat {
  type Output = Self;

  fn sub(self, m2: Self) -> Self {
    (self.v1 - m2.v1, self.v2 - m2.v2).into()
  }
}

impl Mul for Mat {
  type Output = Self;

  fn mul(self, m2: Self) -> Self {
    (
      (
        self.v1.x * m2.v1.x + self.v1.y * m2.v2.x,
        self.v1.x * m2.v1.y + self.v1.y * m2.v2.y,
      ),
      (
        self.v2.x * m2.v1.x + self.v2.y * m2.v2.x,
        self.v2.x * m2.v1.y + self.v2.y * m2.v2.y,
      )
    ).into()
  }
}

impl From<Vector> for Point {
  fn from(v: Vector) -> Point {
    (v.x as i32, v.y as i32).into()
  }
}

impl Mat {
  pub fn rotation(th: f32) -> Mat {
    (
      ( th.cos(), -th.sin()),
      ( th.sin(),  th.cos())
    ).into()
  }

  pub fn mul_vec(&self, v: Vector) -> Vector {
    (self.v1 * v, self.v2 * v).into()
  }
}

pub fn compute_interior_angle(nsides: u8) -> f32 {
  let sum_interior = (std::f32::consts::PI) * ((nsides - 2) as f32);
  sum_interior / (nsides as f32)
}


// pub fn get_rotation_matrix(th: f32) -> Mat {
//   ((th.cos(), th.sin()), (-th.sin(), th.cos()))
// }

// pub fn mul_vec_mat(m: Mat, v: Vector) -> Vector {
//   ((m.0).x * v.x + (m.0).y * v.y, (m.1).x * v.x + (m.1).y * v.y)
// }

// pub fn mul_mat_mat(m1: Mat, m2: Mat) -> Mat {
//   (
//     (
//       (m1.0).x * (m2.0).x + (m1.0).y * (m2.1).x,
//       (m1.0).x * (m2.0).y + (m1.0).y * (m2.1).y
//     ),
//     (
//       (m1.1).x * (m2.0).x + (m1.1).y * (m2.1).x,
//       (m1.1).x * (m2.0).y + (m1.1).y * (m2.1).y
//     )
//   )
// }
